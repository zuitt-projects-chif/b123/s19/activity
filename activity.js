/*

	Activity:

	Update and Debug the following codes to ES6.
		Use template literals,
		Use array/object destructuring,
		Use arrow functions
	
	Create a class constructor able to receive 3 arguments
		-It should be able to receive two strings and a number
		-Using the this keyword assign properties:
			name, 
			breed, 
			dogAge = <7x human years> 
			-assign the parameters as values to each property.

	Create 2 new objects using our constructor.

	This constructor should be able to create Dog objects.

	Log the 2 new Dog objects in the console.


*/
const student1 = {
  name: "Shawn Michaels",
  birthday: "May 5, 2003",
  age: 18,
  isEnrolled: true,
  classes: ["Philosphy 101", "Social Sciences 201"],
};

const student2 = {
  name: "Steve Austin",
  birthday: "June 15, 2001",
  age: 20,
  isEnrolled: true,
  classes: ["Philosphy 401", "Natural Sciences 402"],
};

function introduce({ name, age, classes }) {
  console.log(`Hi! I'm ${name}. I am ${age} years old.`);
  console.log(`I study the following courses ${classes}`);
}
introduce(student1);
introduce(student2);

const getCube = (num) => Math.pow(num, 3);

const cube = getCube(3);

console.log("cube", cube);

const numArr = [15, 16, 32, 21, 21, 2];

console.group("numArr forEach console.log");
numArr.forEach((num) => console.log(num));
console.groupEnd();

const numSquared = numArr.map((num) => num ** 2);

console.log("numSquared", numSquared);

class Dog {
  constructor(name, breed, dogAge) {
    this.name = name;
    this.breed = breed;
    this.dogAge = dogAge * 7;
  }
}

const dog1 = new Dog("Rex", "Rottweiler", 4);
const dog2 = new Dog("Blaze", "Pug", 6.5);

console.log(dog1);
console.log(dog2);
